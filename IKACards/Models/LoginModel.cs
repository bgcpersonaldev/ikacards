﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKACards.Models
{
    public class LoginModel: NotifyBase
    {
        private string _user;
        private string _password;

        public string User
        {
            get
            {
                return _user;
            }//Fin de get.
            set
            {
                _user = value;
                OnPropertyChanged("User");
                //OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Id.

        public string Password
        {
            get
            {
                return _password;
            }//Fin de get.
            set
            {
                _password = value;
                OnPropertyChanged("Password");
               // OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Id.

        
    }
}
