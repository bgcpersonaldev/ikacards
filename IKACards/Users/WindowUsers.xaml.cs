﻿using IKA.Business.Main.Services.Security;
using IKA.Data.Main.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IKACards.Users
{
    /// <summary>
    /// Interaction logic for WindowUsers.xaml
    /// </summary>
    public partial class WindowUsers : UserControl, INotifyPropertyChanged
    {
        private TblUser _Seluser;
        public SecureService usrService;

        public TblUser Seluser
        {
            get { return _Seluser; }
            set
            {
                _Seluser = value;
                OnPropertyChanged("Seluser");
            }
        }

        public WindowUsers()
        {
            InitializeComponent();
            usrService = new SecureService();
            this.Loaded += WindowUsers_Loaded;
        }

        private void WindowUsers_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutGrid.DataContext = this;
            LoadData();
        }

        private void LoadData()
        {
            
            gridusr.ItemsSource = usrService.GetUsers();
        }


        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var x = new WindowsAddUser();
            x.ShowDialog();
            LoadData();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (Seluser == null)
            {

                MessageBox.Show("Debe Seleccionar un registro", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            var x = new WindowEditUser(Seluser);           
            
            x.ShowDialog();
            LoadData();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (Seluser == null)
            {

                MessageBox.Show("Debe Seleccionar un registro", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }
            if (MessageBox.Show("Desea realmente eliminar el registro seleccionado?", "Eliminar",MessageBoxButton.YesNoCancel,MessageBoxImage.Question) == MessageBoxResult.Yes && Seluser != null)
            {

                var res = usrService.DeleteUser(Seluser.Id);
                if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
                {
                    MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                LoadData();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
