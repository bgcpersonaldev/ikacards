﻿using IKA.Business.Main.Services.Security;
using IKA.Data.Main.EF;
using IKACards.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IKACards.Users
{
    /// <summary>
    /// Interaction logic for WindowEditUser.xaml
    /// </summary>
    public partial class WindowEditUser : Window, INotifyPropertyChanged
    {
        private long _Id;
        private string _nick;
        private string _nombre;
        private string _apellido;
        private string _correo;
        private TblRol _selrol;

        public string Nick
        {
            get { return _nick; }
            set
            {
                _nick = value;
                OnPropertyChanged("Nick");
            }
        }

        public string Nombre
        {
            get { return _nombre; }
            set
            {
                _nombre = value;
                OnPropertyChanged("Nombre");
            }
        }

        public string Apellido
        {
            get { return _apellido; }
            set
            {
                _apellido = value;
                OnPropertyChanged("Apellido");
            }
        }

        public string Correo
        {
            get { return _correo; }
            set
            {
                _correo = value;
                OnPropertyChanged("Correo");
            }
        }

        //SelRol

        public TblRol SelRol
        {
            get { return _selrol; }
            set
            {
                _selrol = value;
                OnPropertyChanged("SelRol");
            }
        }

        public SecureService usrService;


        public WindowEditUser(TblUser Seluser)
        {
            InitializeComponent();
            LayoutGrid.DataContext = this;
            usrService = new SecureService();
            var lista= usrService.GetRoles();
            ComboRoles.ItemsSource = lista;
            _Id = Seluser.Id;
            Nick = Seluser.nick;
            Nombre = Seluser.nombre;
            Apellido = Seluser.apellido;
            Correo = Seluser.correo;
            SelRol = lista.FirstOrDefault(a => a.RolId == Seluser.Rolid);
            this.Loaded += WindowEditUser_Loaded;
            
        }

        private void WindowEditUser_Loaded(object sender, RoutedEventArgs e)
        {

            //this.Nick = "prueba";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(Correo))
            {
                if (ValidatorBgc.EmailIsValid(Correo))
                {
                    chkinfoemail.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("No tiene un correo válido", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (!(bool)checkBox.IsChecked)
            {
                if (String.IsNullOrEmpty(TxtPassword.Password) || (TxtPassword.Password != TxtPassword1.Password))
                {
                    MessageBox.Show("Las contraseñas debe coincidir", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                var res = usrService.EditUserFull(new IKA.Business.Main.DTO.TblUserDto()
                {
                    Id=_Id,
                    Nick = Nick,
                    apellido = Apellido,
                    nombre = Nombre,
                    correo = Correo,
                    fechaCreacion = DateTime.Now.ToString(),
                    Rolid = SelRol.RolId,
                    Rolname = SelRol.RolName,
                    password = TxtPassword.Password
                });

                if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
                {
                    MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
               
                var res = usrService.EditUser(new IKA.Business.Main.DTO.TblUserDto()
                {
                    Id = _Id,
                    Nick = Nick,
                    apellido = Apellido,
                    nombre = Nombre,
                    correo = Correo,
                    fechaCreacion = DateTime.Now.ToString(),
                    Rolid = SelRol.RolId,
                    Rolname = SelRol.RolName,
                    password = TxtPassword.Password
                });

                if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
                {
                    MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {
                    this.Close();
                }
            }

           
            
            
        }
    }
}
