﻿using IKA.Business.Main.Services.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace IKACards
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string errorMessage = string.Format("Se ha encontrado un error en la aplicación: {0}", e.Exception.Message);
            MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
            //falta definir el logeo del error
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //primero hay que verificar que este activo el sistema

            var servicio = new SecureService();
            var validacion = servicio.VerifySoftwareActivation();
            if (validacion.TypeEnum==IKA.Business.Main.DTO.TiposM.Error)
            {
                //
            }
            //en el incio de la aplicacion
            StartupUri = new Uri("MainWindow.xaml", System.UriKind.Relative);
        }
    }
}
