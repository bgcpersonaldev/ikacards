﻿using IKACards.roles;
using IKACards.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IKACards
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
            
        }

        private void RibbonRadioButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var wd = new WindowUsers();
                contenedor.Content = wd;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void RibbonRadioButton_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var wd = new WindowRoles();
                contenedor.Content = wd;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
