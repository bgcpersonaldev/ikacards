﻿using IKACards.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IKACards.ViewModel
{
    public class LoginViewModel: LoginModel, INotifyPropertyChanged
    {
        private ICommand _LoginCommand;
        private ICommand _CancelCommand;



        public LoginViewModel()
        {
            LoginCommand = new CommandBase(param => this.loginClient());
            CancelCommand = new CommandBase(param => this.CancelClient());
        }

        public ICommand LoginCommand
        {
            get {
                return _LoginCommand;
            }
            set
            {
                _LoginCommand = value;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return _CancelCommand;
            }
            set
            {
                _CancelCommand = value;
            }
        }

        private void loginClient()
        {
            Console.WriteLine("esto llego" + this.User);
            
        }

        private void CancelClient()
        {
            Console.WriteLine("esto llego");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }//Fin de if.
        }//Fin de OnPropertyChanged.
    }
}
