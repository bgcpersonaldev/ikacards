﻿using IKA.Business.Main.Services.Security;
using IKA.Data.Main.EF;
using IKACards.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IKACards.roles
{
    /// <summary>
    /// Interaction logic for WindowRoles.xaml
    /// </summary>
    public partial class WindowRoles : UserControl, INotifyPropertyChanged
    {
        private TblRol _SelItem;
        public SecureService usrService;

        public TblRol SelItem
        {
            get { return _SelItem; }
            set
            {
                _SelItem = value;
                OnPropertyChanged("Seluser");
            }
        }

        public WindowRoles()
        {
            InitializeComponent();
            usrService = new SecureService();
            this.Loaded += WindowRoles_Loaded;
        }

        private void LoadData()
        {
            LayoutGrid.DataContext = this;
            gridrol.ItemsSource = usrService.GetRoles();
        }

        private void WindowRoles_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var x = new WindowAddRole();
            x.ShowDialog();
            LoadData();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (SelItem != null)
            {
                var x = new WindowEditRole(SelItem);
                x.ShowDialog();
                LoadData();
            }
            else
            {
                MessageBox.Show("Debe Seleccionar un registro", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (SelItem != null)
            {
                if (MessageBox.Show("Desea realmente eliminar el registro seleccionado?", "Eliminar", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var res = usrService.DeleteRole(SelItem.RolId);
                    if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
                    {
                        MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    LoadData();
                }
            }
            else
            {
                MessageBox.Show("Debe Seleccionar un registro", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
