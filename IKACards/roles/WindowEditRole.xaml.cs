﻿using IKA.Business.Main.Services.Security;
using IKA.Data.Main.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit.Core;

namespace IKACards.roles
{
    /// <summary>
    /// Interaction logic for WindowEditRole.xaml
    /// </summary>
    public partial class WindowEditRole : Window, INotifyPropertyChanged
    {
        private TblRol _selrol;
        private string _RolName;
        public SecureService usrService;

        public TblRol SelRol
        {
            get { return _selrol; }
            set
            {
                _selrol = value;
                OnPropertyChanged("SelRol");
            }
        }

        public string RolName
        {
            get { return _RolName; }
            set
            {
                _RolName = value;
                OnPropertyChanged("RolName");
            }
        }

        public WindowEditRole(TblRol selRol)
        {
            InitializeComponent();
            usrService = new SecureService();

            LayoutGrid.DataContext = this;
            SelRol = selRol;
            this.Loaded += WindowEditRole_Loaded;
        }

        private void WindowEditRole_Loaded(object sender, RoutedEventArgs e)
        {
            var act = getActionsfromint(SelRol.RolActions);


            if (act[0] == '1')
            {
                Admusuario.IsChecked = true;
            }
            else
            {
                Admusuario.IsChecked = false;
            }

            if (act[1] == '1')
            {
                Admcontra.IsChecked = true;
            }
            else
            {
                Admcontra.IsChecked = false;
            }

            if (act[2] == '1')
            {
                AdmcTarjeta.IsChecked = true;
            }
            else
            {
                AdmcTarjeta.IsChecked = false;
            }

            if (act[3] == '1')
            {
                AdmdTarjeta.IsChecked = true;
            }
            else
            {
                AdmdTarjeta.IsChecked = false;
            }

            RolName = SelRol.RolName;
        }

        public string getActionsfromint(long valor)
        {
            string binary = Convert.ToString(valor, 2);
            for (var i = 0; binary.Length < 4; i++)
            {
                binary = "0" + binary;
            }
            return binary;
        }

        public long GetActionCode()
        {
            var res = 0;
            var permiso = "1234";

            #region [ Definicion facil de un permiso ]


            if (Admusuario.IsChecked == true)
            {
                permiso = permiso.Replace("1", "1");
            }
            else
            {
                permiso = permiso.Replace("1", "0");
            }

            if (Admcontra.IsChecked == true)
            {
                permiso = permiso.Replace("2", "1");
            }
            else
            {
                permiso = permiso.Replace("2", "0");
            }

            if (AdmcTarjeta.IsChecked == true)
            {
                permiso = permiso.Replace("3", "1");
            }
            else
            {
                permiso = permiso.Replace("3", "0");
            }

            if (AdmdTarjeta.IsChecked == true)
            {
                permiso = permiso.Replace("4", "1");
            }
            else
            {
                permiso = permiso.Replace("4", "0");
            }

            #endregion

            res = Convert.ToInt32(permiso, 2);

            return res;
        }




        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(RolName))
            {

                MessageBox.Show("No tiene nombre de rol válido", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }


            var res = usrService.EditRolFull(new IKA.Business.Main.DTO.TblRolDto()
            {
                Rolid = SelRol.RolId,
                RolName = RolName,
                RolActions = GetActionCode(),
                RolCreationUser = "UsrTmp"
            });

            if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
            {
                MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                this.Close();
            }
        }
    }

}
