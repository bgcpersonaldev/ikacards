﻿using IKA.Business.Main.Services.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IKACards.roles
{
    /// <summary>
    /// Interaction logic for WindowAddRole.xaml
    /// </summary>
    public partial class WindowAddRole : Window, INotifyPropertyChanged
    {
        private string _RolName;
        public SecureService usrService;

        public string RolName
        {
            get { return _RolName; }
            set
            {
                _RolName = value;
                OnPropertyChanged("RolName");
            }
        }

        public WindowAddRole()
        {
            InitializeComponent();
            usrService = new SecureService();
            LayoutGrid.DataContext = this;
        }

        public long GetActionCode()
        {
            var res = 0;
            var permiso = "1234";

            #region [ Definicion facil de un permiso ]


            if (Admusuario.IsChecked == true)
            {
                permiso=permiso.Replace("1", "1");
            }
            else
            {
                permiso = permiso.Replace("1", "0");
            }

            if (Admcontra.IsChecked == true)
            {
                permiso = permiso.Replace("2", "1");
            }
            else
            {
                permiso = permiso.Replace("2", "0");
            }

            if (AdmcTarjeta.IsChecked == true)
            {
                permiso = permiso.Replace("3", "1");
            }
            else
            {
                permiso = permiso.Replace("3", "0");
            }

            if (AdmdTarjeta.IsChecked == true)
            {
                permiso = permiso.Replace("4", "1");
            }
            else
            {
                permiso = permiso.Replace("4", "0");
            }

            #endregion

            res = Convert.ToInt32(permiso, 2);

            return res;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(RolName))
            {

                MessageBox.Show("No tiene nombre de rol válido", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }


            var res = usrService.AddRolFull(new IKA.Business.Main.DTO.TblRolDto()
            {
                RolName = RolName,
                RolCreationDate = DateTime.Now.ToString(),
                RolActions = GetActionCode(),
                RolCreationUser = "UsrTmp"
            });

            if (res.TypeEnum != IKA.Business.Main.DTO.TiposM.ResultOk)
            {
                MessageBox.Show(res.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }






        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
