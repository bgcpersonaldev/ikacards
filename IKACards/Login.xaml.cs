﻿using IKA.Business.Main.Services;
using IKACards.Models;
using IKACards.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IKACards
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window,  INotifyPropertyChanged
    {
        private string _user;
        private string _password;
        private ICommand _LoginCommand;
        private ICommand _CancelCommand;


        public string User
        {
            get
            {
                return _user;
            }//Fin de get.
            set
            {
                _user = value;
                OnPropertyChanged("User");
                //OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Id.

        public string Password
        {
            get
            {
                return _password;
            }//Fin de get.
            set
            {
                _password = value;
                OnPropertyChanged("Password");
                // OnPropertyChanged("DisplayName");
            }//Fin de set.
        }//Fin de propiedad Id.

        public Login()
        {
            InitializeComponent();
            GridRoot.DataContext = this;
            LoginCommand = new CommandBase(param => this.loginClient(param));
            CancelCommand = new CommandBase(param => this.CancelClient());
        }

        public ICommand LoginCommand
        {
            get
            {
                return _LoginCommand;
            }
            set
            {
                _LoginCommand = value;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return _CancelCommand;
            }
            set
            {
                _CancelCommand = value;
            }
        }

        private void loginClient(object txtpass)
        {
            var x = txtpass as PasswordBox;
            if (x == null)
            {
                return;
            }
            this.Password = x.Password;

            var servicio = new LoginService();
            var res = servicio.Login(this.User, this.Password);


        }

        private void CancelClient()
        {
            Console.WriteLine("esto llego");
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }//Fin de if.
        }//Fin de OnPropertyChanged.
    }
}
