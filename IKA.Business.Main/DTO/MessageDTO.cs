﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKA.Business.Main.DTO
{
    public class MessageDTO
    {
        public TiposM TypeEnum { set; get; }
        public string Message { get; set; }

        public MessageDTO()
        {
            this.TypeEnum = TiposM.ResultOk;
        }

        public void AddError(string message)
        {
            this.TypeEnum = TiposM.Error;
            this.Message = message;
        }

    }

    public enum TiposM
    {
        Error,ResultOk 
    }
}
