﻿using IKA.Common.Main.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKA.Business.Main.DTO
{
    public class TblUserDto
    {
        private string _nick;
        private string _nombre;
        private string _apellido;
        private string _correo;
        private string _password;
        private string _fechacreacion;

        public Nullable<long> Rolid { get; set; }


        public long Id { get; set; }
        public string Rolname { get; set; }

        public string Nick {
            get
            {
                return _nick;
            }
            set
            {
                _nick = value.Sanitize();
            }
        }
        public string nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = value.Sanitize();
            }
        }
        public string apellido
        {
            get
            {
                return _apellido;
            }
            set
            {
                _apellido = value.Sanitize();
            }
        }
        public string correo
        {
            get
            {
                return _correo;
            }
            set
            {
                _correo = value.Sanitize();
            }
        }
        public string password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value.Sanitize();
            }
        }
        public string fechaCreacion
        {
            get
            {
                return _fechacreacion;
            }
            set
            {
                _fechacreacion = value.Sanitize();
            }
        }
        

    }
}
