﻿using IKA.Common.Main.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKA.Business.Main.DTO
{
    public class TblRolDto
    {
        private string _RolName;


        public Nullable<long> Rolid { get; set; }
        public string RolCreationDate { get; set; }
        public long RolActions { get; set; }
        public string RolCreationUser { get; set; }
        public string RolUpdateUser { get; set; }


        public string RolName
        {
            get
            {
                return _RolName;
            }
            set
            {
                _RolName = value.Sanitize();
            }
        }
                
    }
}
