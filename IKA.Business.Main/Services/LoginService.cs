﻿using IKA.Business.Main.DTO;
using IKA.Common.Main.Utils;
using IKA.Data.Main.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKA.Business.Main.Services
{
    public class LoginService
    {
        public MessageDTO Login(string user, string password)
        {
            var result = new MessageDTO();

            user = user.Sanitize();
            password = password.Sanitize();
            password = password.EncryptMD5();


            #region [ Validacion ]
            if (user.IsNullOrEmpty())
            {
                result.AddError("El usuario no puede ser nulo");
                return result;
            }

            if (password.IsNullOrEmpty())
            {
                result.AddError("La contraseña no puede ser nula");
                return result;
            }

            #endregion

            using (var db = new mainEntities())
            {
                var targetUser = db.TblUser.FirstOrDefault(a => a.nick == user && a.password == password);
                if (targetUser != null)
                {
                    return result;
                }
                result.AddError("Error al ingresar a la aplicación. La informacion de usuario y contraseña es erronea");
                

            }
            return result;



        }
    }
}
