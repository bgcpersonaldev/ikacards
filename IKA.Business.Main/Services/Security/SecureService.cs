﻿using IKA.Business.Main.DTO;
using IKA.Common.Main.Utils;
using IKA.Data.Main.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IKA.Business.Main.Services.Security
{
    public class SecureService
    {
        public MessageDTO VerifySoftwareActivation()
        {
            var res = new MessageDTO();
            try
            {
                using (var db = new mainEntities())
                {

                }
            }
            catch (Exception ex)
            {

               
            }
            return res;
        }

        public List<TblUser> GetUsers()
        {
            var res = new List<TblUser>();
            using (var db = new mainEntities())
            {
                res = db.TblUser.ToList();
            }
            return res;
        }

        public List<TblRol> GetRoles()
        {
            var res = new List<TblRol>();
            using (var db = new mainEntities())
            {
                res = db.TblRol.ToList();
            }
            return res;
        }

        /// <summary>
        /// Añade un nuevo usuario al sistema
        /// </summary>
        /// <returns></returns>
        public MessageDTO AddNewUser(TblUserDto user)
        {
            var res = new MessageDTO();
            try
            {
                #region [ Validaciones]
                if (user.Nick.IsNullOrEmpty())
                {
                    res.AddError("El usuario no puede ser vacio");
                    return res;
                }

                if (user.password.IsNullOrEmpty())
                {
                    res.AddError("El password no puede ser vacio");
                    return res;
                }

                if (user.correo.IsNullOrEmpty())
                {
                    res.AddError("El corrreo no puede ser vacio");
                    return res;
                }
                var fecha = DateTime.Now;
                #endregion
                using (var db = new mainEntities())
                {

                    var usertarget = new TblUser()
                    {
                        
                        nick = user.Nick,
                        password = user.password.EncryptMD5(),
                        fechaCreacion = fecha.ToString(),
                        nombre = user.nombre,
                        apellido = user.apellido,
                        correo = user.correo,
                        Rolid = user.Rolid
                    };
                    db.TblUser.Add(usertarget);
                    db.SaveChanges();
                    
                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO EditUser(TblUserDto user)
        {
            var res = new MessageDTO();
            try
            {
                #region [ Validaciones]
                if (user.Nick.IsNullOrEmpty())
                {
                    res.AddError("El usuario no puede ser vacio");
                    return res;
                }

                if (user.correo.IsNullOrEmpty())
                {
                    res.AddError("El corrreo no puede ser vacio");
                    return res;
                }
                var fecha = DateTime.Now;
                #endregion
                using (var db = new mainEntities())
                {
                    var targetuser = db.TblUser.FirstOrDefault(a => a.Id == user.Id);

                    targetuser.nombre = user.nombre;
                    targetuser.apellido = user.apellido;
                    targetuser.correo = user.correo;
                    targetuser.Rolid = user.Rolid;
                    targetuser.fechaUltMod = fecha.ToString();
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO EditUserFull(TblUserDto user)
        {
            var res = new MessageDTO();
            try
            {
                #region [ Validaciones]
                if (user.Nick.IsNullOrEmpty())
                {
                    res.AddError("El usuario no puede ser vacio");
                    return res;
                }

                if (user.password.IsNullOrEmpty())
                {
                    res.AddError("El password no puede ser vacio");
                    return res;
                }

                if (user.correo.IsNullOrEmpty())
                {
                    res.AddError("El corrreo no puede ser vacio");
                    return res;
                }
                var fecha = DateTime.Now;
                #endregion
                using (var db = new mainEntities())
                {
                    var targetuser = db.TblUser.FirstOrDefault(a => a.Id == user.Id);

                    targetuser.password = user.password.EncryptMD5();
                    
                    targetuser.nombre = user.nombre;
                    targetuser.apellido = user.apellido;
                    targetuser.correo = user.correo;
                    targetuser.Rolid = user.Rolid;
                    targetuser.fechaUltMod = fecha.ToString();
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO DeleteUser(long userId)
        {
            var res = new MessageDTO();
            try
            {
                using (var db = new mainEntities())
                {
                    var targetuser = db.TblUser.FirstOrDefault(a => a.Id == userId);
                    db.TblUser.Remove(targetuser);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO AddRolFull(TblRolDto rol)
        {
            var res = new MessageDTO();
            try
            {
                #region [ Validaciones]
                if (rol.RolName.IsNullOrEmpty())
                {
                    res.AddError("El rol no puede ser vacio");
                    return res;
                }


                var fecha = DateTime.Now;
                #endregion
                using (var db = new mainEntities())
                {
                    var newrol = new TblRol()
                    {
                        RolName=rol.RolName,
                        RolActions = rol.RolActions,
                        RolCreationDate = fecha.ToString(),
                        RolCreationUser = rol.RolCreationUser
                    };

                    db.TblRol.Add(newrol);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO EditRolFull(TblRolDto rol)
        {
            var res = new MessageDTO();
            try
            {
                #region [ Validaciones]
                if (rol.RolName.IsNullOrEmpty())
                {
                    res.AddError("El rol no puede ser vacio");
                    return res;
                }

                                
                var fecha = DateTime.Now;
                #endregion
                using (var db = new mainEntities())
                {
                    var target = db.TblRol.FirstOrDefault(a => a.RolId == rol.Rolid);

                    target.RolName = rol.RolName;
                    target.RolActions = rol.RolActions;
                    target.fechaUltMod = fecha.ToString();
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO DeleteRole(long rolId)
        {
            var res = new MessageDTO();
            try
            {
                using (var db = new mainEntities())
                {
                    var target = db.TblRol.FirstOrDefault(a => a.RolId == rolId);
                    db.TblRol.Remove(target);
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {

                res.AddError(ex.ToString());
            }

            return res;
        }

        public MessageDTO ActiveUser(TblUserDto user)
        {
            var res = new MessageDTO();
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }

            return res;
        }

        public MessageDTO DeactiveUser(TblUserDto user)
        {
            var res = new MessageDTO();
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }

            return res;
        }
    }
}
